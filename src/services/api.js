import AsyncStorage from '@react-native-community/async-storage';
import { create } from 'apisauce';

const api = create({
    baseURL: 'http://10.82.3.95/dev/nextem/public/api',
});
api.addAsyncRequestTransform(request => async() =>{
    const token = await AsyncStorage.getItem('@CodeApi:token');

    if (token) {
        request.headers['Authorization'] = `Bearer ${token}`;
    }
});
api.addResponseTransform(response => {
    if(!response.ok)
    {
        throw response;
    }
});

export default api;