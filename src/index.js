/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, 
        StyleSheet, 
        Text, 
        View,
        Button} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import api from './services/api';

export default class App extends Component {
  state = {
    loggedInUser: null,
    errorMessage: null,
    projects: [],
  };

  signIn = async () => {
    try {
      const response = await api.post('auth/login', {
        email: 'teste@nextem.com.br',
        password: '1234'
      });
      const {user, token} = response.data;
      await AsyncStorage.multiSet([
        ['@CodeApi:token', token],
        ['@CodeApi:user', JSON.stringify(user)],
      ]);
      this.setState({loggedInUser: user});
      alert('Login com sucesso!');
    } catch (response) {
      this.setState({ errorMessage: response.data.error });
    }
  };

  getProjectList = async () => {
    try {
      const response = await api.get('projects');
      console.log(response.data);
      const { projects } = response.data;
      //console.log({ projects });
      this.setState({ projects });
    } catch (response) {
      this.setState({errorMessage: response.data.error});
    }
  };

  async componentDidMount()
  {
    const token = await AsyncStorage.getItem('@CodeApi:token');
    const user = JSON.parse(await AsyncStorage.getItem('@CodeApi:user'));

    if (token && user)
    {
      this.setState({loggedInUser: user});
    }
  }

  render() {
    return (
      <View style={styles.container}>
        { !! this.state.loggedInUser && <Text>{this.state.loggedInUser}</Text> }
        { !! this.state.errorMessage && <Text>{this.state.errorMessage}</Text> }
        {this.state.loggedInUser ? 
          <Button onPress={this.getProjectList} title="Carregar" /> 
          :
          <Button onPress={this.signIn} title="Entrar" />
        }
        {
          this.state.projects.map(project => (
            <View key={project.id} style={{marginTop: 15}}>
              <Text style={{fontWeight: 'bold'}}>
                {project.name}
              </Text>
            </View> 
          ))
        }
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

